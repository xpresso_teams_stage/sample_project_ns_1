import lightgbm as lgb
import matplotlib.pyplot as plt
import numpy as np
from sklearn.utils.multiclass import type_of_target

from xpresso.ai.core.commons.utils.constants import EMPTY_STRING
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.ml_utils.generic import REGRESSOR, CLASSIFIER, \
    CLASSIFIER_TARGET_TYPES, BINARY, format_metric_key, PLOTS_FOLDER
from xpresso.ai.core.ml_utils.sklearn import SklearnMetrics
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot


class LightgbmMetrics(SklearnMetrics):
    def __init__(self, model, parameters_json=None):
        super().__init__(model, parameters_json)

    def validate_model_type(self):
        if not (isinstance(self.model, lgb.Booster) or
                isinstance(self.model, lgb.sklearn.LGBMModel)):
            return False
        return True

    def report_training_best_iteration(self):
        """

        Identify and report best iteration and best score during LightGBM
        model training, support when the model is
        trained with LightGBM Scikit-Learn API

        """
        # datatype check : if model is a LightGBM model trained with
        # Scikit-Learn API
        if not isinstance(self.model, lgb.sklearn.LGBMModel):
            return
        try:
            for k, v in self.model.best_score_.items():
                for m_name, m_value in v.items():
                    self.calculated_metrics[
                        f"Best Score ({format_metric_key(m_name)}) During "
                        f"Training"] = round(m_value, 2)
        except Exception as exp:
            self.logger.exception(f"Unable to calculate metric."
                                  f"Reason: {exp}")

        try:
            self.calculated_metrics[
                "Best Iteration During Training"] = self.model.best_iteration_
        except Exception as exp:
            self.logger.exception(f"Unable to calculate best iteration "
                                  f"metric. Reason: {exp}")

    def predict_and_evaluate(self, x_values, y_values,
                             prediction_threshold, dataset_name=EMPTY_STRING,
                             model_type=EMPTY_STRING):
        """
        Args:
            x_values: array_like or sparse matrix, shape (n_samples, n_features)
                input matrix (either training set, validation set, or test set)
            y_values: array-like of shape (n_samples,) or (n_samples, n_classes)
                target variable vector or matrix (either training set,
                validation set, or test set)
            dataset_name: string
                 The value will be "tr" and "val", indicating training set,
                validation set, if generate_validation_metrics
                is True.
            model_type : String (Optinal) ["regressor", "classifier"] If no
                value is given, the model type will be inferred from model
                loss or the data type of target variable (y) using
                sklearn.utils.multiclass.type_of_target method.
            prediction_threshold : float (default = 0.5) Prediction threshold
                that is compared to prediction probability output by tensorflow
                model.predict() when determining the prediction label for
                classification model.
        """
        self.metric_suffix = dataset_name
        # identify model_type
        self.set_model_type(y_values, model_type)
        # identify target_type
        self.set_target_type(y_values)

        self.report_training_best_iteration()

        # datatype check : if model is LightGBM model trained with
        # Scikit-Learn API
        if isinstance(self.model, lgb.sklearn.LGBMModel):
            # plot learning curve of the model training process
            lightgbm_plot_utils = LightgbmPlots()
            lightgbm_plot_utils.plot_learning_curve(self.model)
        # make predictions
        y_pred, y_pred_proba = self.generate_prediction(x_values, y_values)
        y_pred = self.get_y_pred_label(y_pred, prediction_threshold)

        # populate metrics
        self.populate_metrics(x_values, y_values, y_pred, y_pred_proba, None)

    def set_model_type(self, y_values, model_type=EMPTY_STRING):
        """
                Determine the model is a regressor or classifier.
                If user provides True/False to is_regressor parameter,
                apply user's
                input. If not, infer model type
                based on the data type of target variable with Sklearn
                type_of_target
                method

                """

        lgb_objectives_regr = ["regression", "regression_l2", "l2",
                               "mean_squared_error", "mse", "l2_root",
                               "root_mean_squared_error",
                               "root_mean_squared_error",
                               "regression_l1", "l1", "mean_absolute_error",
                               "mae",
                               "mean_absolute_error", "fair", "poisson",
                               "quantile",
                               "mape", "mean_absolute_percentage_error",
                               "gamma", "tweedie"]
        lgb_objectives_clf = ["binary",
                              "multiclass",
                              "multiclassova",
                              "cross_entropy",
                              "xentropy",
                              "cross_entropy_lambda",
                              "xentlambda"]

        lgb_metrics_regr = ["l1", "mean_absolute_error", "mae", "l2",
                            "mean_squared_error", "mse", "regression_l2",
                            "regression",
                            "rmse", "root_mean_squared_error", "l2_root",
                            "quantile", "mape",
                            "mean_absolute_percentage_error",
                            "huber", "fair", "poisson", "gamma",
                            "gamma_deviance", "tweedie"]

        lgb_metrics_clf = ["map", "mean_average_precision", "auc",
                           "binary_logloss", "binary", "binary_error",
                           "auc_mu", "multi_logloss", "multiclass", "softmax",
                           'multiclassova', "multiclass_ova", "ova", "ovr",
                           "multi_error", "cross_entropy", "xentropy",
                           "cross_entropy_lambda", "xentlambda",
                           "kullback_leibler", "kldiv"]

        if model_type and model_type.lower() in [REGRESSOR, CLASSIFIER]:
            self.model_type = model_type
        # if user doesn't specify model type, try identify model type from
        # model objective or evaluation metrics
        # from the parameters of model
        elif isinstance(self.model, lgb.LGBMRegressor):
            self.model_type = REGRESSOR
        elif isinstance(self.model, lgb.LGBMClassifier):
            self.model_type = CLASSIFIER
        else:
            params_metrics, params_objective = \
                self.identify_params_obj_metrics()
            # first try identifying model type from model objective
            if params_objective:
                try:
                    if params_objective in lgb_objectives_clf:
                        self.model_type = CLASSIFIER
                    elif params_objective in lgb_objectives_regr:
                        self.model_type = REGRESSOR
                except Exception:
                    self.logger.exception("Unable to determine model type "
                                          "from losses")
            # then try identifying model type from model evaluation metrics
            elif params_metrics:
                try:
                    if any(mtr in lgb_metrics_clf for mtr in params_metrics):
                        self.model_type = CLASSIFIER
                    elif any(mtr in lgb_metrics_regr for mtr in params_metrics):
                        self.model_type = REGRESSOR
                except Exception:
                    self.logger.exception("Unable to determine model type "
                                          "from losses")
        # finally use sklearn type_of_target method to identify data type of
        # target variable
        if self.model_type not in [CLASSIFIER, REGRESSOR]:
            self.set_target_type(y_values)
            if self.target_type.startswith("continuous"):
                self.model_type = REGRESSOR
            elif self.target_type in CLASSIFIER_TARGET_TYPES:
                self.model_type = CLASSIFIER

    def identify_params_obj_metrics(self):
        """
        Identify objectives and evaluation metrics from the model parameters

        """
        params_metrics = None
        params_objective = None
        if isinstance(self.model, lgb.Booster):
            try:
                params_objective = self.model.params['objective']
                params_metrics = self.model.params['metric']
            except Exception as exp:
                self.logger.exception(f"Reason: {exp}")

        elif isinstance(self.model, lgb.sklearn.LGBMModel):
            try:
                params_objective = self.model.objective_
                # collect name of evaluation metrics from model.evals_results_
                params_metrics = []
                for val_sets, e_mtrs in self.model.evals_result_.items():
                    for e_mtr_name, e_mtr_vals in e_mtrs.items():
                        params_metrics.append(e_mtr_name)
            except Exception as exp:
                self.logger.exception(f"Reason: {exp}")
        return params_metrics, params_objective

    def get_y_pred_label(self, y_pred, prediction_threshold):
        if (type_of_target(y_pred).startswith("continuous") or
                self.target_type.startswith("continuous")):
            y_pred = (y_pred > prediction_threshold).astype(int)
        return y_pred

    def generate_prediction(self, x_values, y_values):
        """
        Generate prediction using model.predict()
        Returns: Prediction, the ooutput from running LightGBM model.predict(
        ) (labeled as y_pred).
                Additional, predicted probabilities if the classification
                model has attribute of predict_proba.
        """
        try:
            # Apply trained model to make predictions on given dataset
            y_pred = self.model.predict(x_values)
            y_pred_proba = None
            # Check if the trained model can predict probability
            if hasattr(self.model, "predict_proba"):
                y_pred_proba = self.model.predict_proba(x_values)

            # if objective is binary:logistic, the output of model.predict()
            # is probabilities not the exact label
            if (self.target_type in CLASSIFIER_TARGET_TYPES and
                    (self.target_type.startswith("continuous"))):
                y_pred_proba = y_pred
                # reshape y_pred_proba array
                if self.target_type == BINARY:
                    y_pred_proba = np.c_[1 - y_pred, y_pred]
            return y_pred, y_pred_proba
        except Exception as exp:
            self.logger.exception(
                f"Unable to generate predictions. Reason:{exp}")


class LightgbmPlots:
    def __init__(self):
        self.logger = XprLogger(name="LightgbmPlots")

    def plot_learning_curve(self, model=None, evals_result=None):
        """
        Generate learning curves for a trained LightGBM. model: the test,
        train scores vs num_boost_round.

        Users can choose to pass in either a trained LightGBM model trained
        with Scikit-Learn API (lightgbm.LGBMRegressor
         or lightgbm.LGBMClassifier or the evaluation results (in dictionary)
         returned by training a LightGBM
        Booster model (trained with LightGBM Training API lightgbm.train()
        and parameter evals_result is specified)

        Parameters
        ----------
        evals_result : a dictionary containing evaluation results returned
        from lightgbm.train()
                     (model is trained using LightGBM Training API
                     lightgbm.train() and parameter evals_result is
                     specified)

        """
        # if the trained model is a LGBMModel instance, pass in the model
        # if the model is Booster(lightgbm.train()), use the Dictionary
        # returned from lightgbm.train()

        if (model is not None) and (isinstance(model, lgb.sklearn.LGBMModel)):
            evals_result_dict = model.evals_result_
            plot_metric_booster = model
        elif (evals_result is not None) and (isinstance(evals_result, dict)):
            evals_result_dict = evals_result
            plot_metric_booster = evals_result
        else:
            evals_result_dict = None
            plot_metric_booster = None

        # identify evaluation metrics used when fitting the lightGBM model
        valid_sets_list = []
        e_mtr_list = []
        for val_sets, e_mtrs in evals_result_dict.items():
            # collect names of validation sets
            valid_sets_list.append(val_sets)
            e_mtr_list = list(e_mtrs.keys())

        # Obtain the unique metrics
        metrics_set = np.unique(e_mtr_list)

        # plot training curve for each metrics
        for e_metric in metrics_set:
            try:
                lgb.plot_metric(plot_metric_booster, e_metric,
                                title=format_metric_key(
                                    e_metric) + " during training")
                plt.show()
                xpresso_save_plot("learning_curve_" + e_metric,
                                  output_path=PLOTS_FOLDER)
                self.logger.info("Learning Curve Saved")
            except Exception as exp:
                self.logger.exception(f"Unable plot learning curve for "
                                      f"lightgbm model. Reason: {exp}")
