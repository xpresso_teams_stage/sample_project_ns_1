"""
    Class design for Presto Connectivity
"""


__all__ = ['PrestoConnector']
__author__ = ['Shlok Chaudhari']


import os
import shutil
import OpenSSL
import urllib3
import requests
import prestodb
import pandas as pd
from xpresso.ai.core.logging.xpr_log import XprLogger
import xpresso.ai.core.commons.utils.constants as constants
import xpresso.ai.core.commons.exceptions.xpr_exceptions as xpr_exp
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser


config = XprConfigParser()
controller_url = config[constants.CONTROLLER_FIELD][constants.SERVER_URL_FIELD]


class PrestoConnector:
    """
        This class is used to interact with any of the databases as per
        the configurations provided. The supported operation as of now are
        importing of data.
    """

    urllib3.disable_warnings(urllib3.exceptions.SubjectAltNameWarning)
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    presto_config = config[constants.connectors][constants.presto]

    ROOT_HOSTS_FILE = presto_config[constants.CONFIG_SSL][constants.root_hosts_path]
    url = f"{controller_url}/ssl_cert/presto"

    def __init__(self, user_input):
        """
            __init__() function initializes the host ip and port for Presto server.
            It also initiatiates the catalog and schema needed for Presto client.
        """
        self.logger = XprLogger()
        self.connector = None
        self.cursor = None
        self.presto_host_name = self.presto_config[constants.presto_hostname]
        self.presto_host_port = self.presto_config[constants.presto_port]
        self.presto_user = self.presto_config[constants.presto_user]
        self.catalog = self.presto_config[constants.DSN][user_input][constants.catalog]
        self.schema = self.presto_config[constants.DSN][user_input][constants.schema]
        self.cert_path = os.path.join(
            os.path.dirname(XprConfigParser().get_default_config_path()),
            self.presto_config[constants.CONFIG_SSL][constants.CONFIG_SSL_CERT_PATH]
        )
        os.makedirs(os.path.dirname(self.cert_path), exist_ok=True)
        self.get_certificate()
        self.set_hosts()

    def get_etc_hostnames(self):
        """
            Helper method to parse /etc/hosts file and
            return all the hostnames in a list.
        """
        hosts = []
        with open(self.ROOT_HOSTS_FILE, 'r') as f:
            hostlines = f.readlines()
        hostlines = [line.strip() for line in hostlines
                     if not line.startswith('#') and line.strip() != '']
        for line in hostlines:
            hostnames = line.split('#')[0].split()[1:]
            hosts.extend(hostnames)
        return hosts

    def read_certificate(self):
        """
            Helper method to read the certificate
        """
        cert = OpenSSL.crypto.load_certificate(
            OpenSSL.crypto.FILETYPE_PEM,
            bytes(open(self.cert_path).read(), 'utf-8')
        )
        return cert.get_subject().OU

    def get_certificate(self):
        """
            Method to get certificate from server to local filesystem
        """
        if os.path.exists(self.cert_path):
            os.remove(self.cert_path)

        resp = requests.get(self.url, verify=False)
        if resp.json()['outcome'] == constants.OUTCOME_SUCCESS:
            open(self.cert_path, 'w').write(resp.json()['results']['cert_text'])
        else:
            message = "Failed to fetch certificate for presto server"
            self.logger.exception(message)
            raise xpr_exp.DBConnectionFailed(message=message)

    def set_hosts(self):
        """
            Method to set hostname in the /etc/hosts
        """
        if self.presto_host_name not in self.get_etc_hostnames():
            presto_ip = self.read_certificate()
            with open(self.ROOT_HOSTS_FILE, 'a') as hosts_file:
                hosts_file.write(f"\n{presto_ip}    {self.presto_host_name}")

    def remove_certificate(self):
        """
            Method to delete certificate from local filesystem
        """
        if os.path.exists(self.cert_path):
            os.remove(self.cert_path)

    def remove_hosts(self):
        """
            Method to remove hostname from the /etc/hosts
        """
        if self.presto_host_name in self.get_etc_hostnames():
            with open(self.ROOT_HOSTS_FILE, 'r') as hosts_file:
                lines = hosts_file.readlines()
                lines = lines[:-2]
            open(self.ROOT_HOSTS_FILE, 'w').write(''.join(lines))

    def get_connector(self):
        """
            This method creates connection to database with the provided configurations.
        """
        try:
            presto_session = prestodb.client.PrestoRequest.http.Session()
            presto_session.verify = self.cert_path
            connector = prestodb.dbapi.connect(
                host=self.presto_host_name, port=self.presto_host_port,
                user=self.presto_user, catalog=self.catalog,
                schema=self.schema, http_scheme=constants.HTTPS
            )
            connector._http_session = presto_session
        except Exception:
            self.logger.exception("Connection to presto client failed.")
            raise xpr_exp.DBConnectionFailed
        return connector

    def import_data(self, table, columns):
        """
            This method makes a select query.
        Args:
            table (str): table name.
            columns (str/list): names of columns as a comma-separated string.
                Put '*' in case of all columns.
        Returns:
             object: a pandas DataFrame.
        """
        self.connector = self.get_connector()
        query = "select %s from %s"
        self.cursor = self.connector.cursor()
        self.cursor.execute(query % (','.join(columns), table))
        results = self.cursor.fetchall()
        columns = []
        for column in self.cursor.description:
            columns.append(column[0])
        self.remove_certificate()
        self.remove_hosts()
        shutil.rmtree(os.path.dirname((os.path.dirname(self.cert_path))))
        return pd.DataFrame(results, columns=columns)

    def close(self):
        """
            Method used to close all connections to the API.
        """
        self.cursor.close()
        self.connector.close()
