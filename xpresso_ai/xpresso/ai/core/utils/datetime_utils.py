from datetime import datetime


class DatetimeUtils:

    @staticmethod
    def convert_time_str_format1_to_datetime_obj(time_in_str: str) -> datetime:
        """
        converts a time object which is stored in string format back to a
        datetime object format
        This method handles the string that follows format 1 ->
            "%A, %B %d %Y, %H:%M:%S"

        Args:
            time_in_str: time input as a string
        Returns:
            time: time as datetime object
        """
        time_in_datetime = \
            datetime.strptime(time_in_str, "%A, %B %d %Y, %H:%M:%S")
        return time_in_datetime

    @staticmethod
    def check_if_date_is_in_30_days(query_date: datetime) -> bool:
        """
        checks if the input date is within 30 days period to current time

        Args:
            query_date: date in query, in  datetime format
        Returns:
            True: if query_date is within 30 days to current time
            False: if query_date is older than 30 days
        """
        time_delay = datetime.now() - query_date
        return time_delay.days <= 30
